function countLetter(letter, sentence) {
    // Check first whether the letter is a single character.
    if (letter.length !==1 || typeof letter !== "string") {
        return undefined;
    }

    let result = 0;
    const lowerCaseLetter = letter.toLowerCase();
    for (const char of sentence) {
        const lowerCaseChar = char.toLowerCase(); 

        if (lowerCaseChar === lowerCaseLetter) {
            result++;
        }
    }

    return result;

}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    const lower_case_text = text.toLowerCase();
    const seen_char = new Set();

    for (const char of lower_case_text) {
        if (seen_char.has(char)){
            return false;
        }
        seen_char.add(char);
    }

    return true;

    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    if (age < 13){
        return undefined;
    } else if (age <= 21 || age >= 65 ){
        const discountedPrice = price * 0.8
        return discountedPrice.toFixed(2);
    } else if (age >= 22 && age <= 64){
        return price.toFixed(2);
    }
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const categories_with_no_stocks = new Set();

    for (const item of items){
        if (item.stocks === 0) {
            categories_with_no_stocks.add(item.category);
        }
    }

    return Array.from(categories_with_no_stocks);

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const common_voters = []

    for (const voter of candidateA) {
        if (candidateB.includes(voter)){
            common_voters.push(voter)
        }
    }

    return common_voters;
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};