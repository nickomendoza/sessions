import Banner from '../components/Banner.js';
import FeaturedCourses from '../components/FeaturedCourses.js';
import Highlights from '../components/Highlights.js';


export default function Home(){
	return(
		<>
			<Banner 
				title="Nicko's Fishing Academy" 
				subtitle = "Fish for Everyone!"
				buttonText ="Enroll Now!"
				buttonLink ="/login"
			/>
			<FeaturedCourses />
			<Highlights/>
		</>
	)
}