import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function AddCourse(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// states
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false);

	function addNewCourse(event) {
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data) {
				Swal.fire({
				    title: "Course Added",
				    icon: "success",
				    text: "You have successfully add new Course."
				})

				setName('');
				setDescription('');
				setPrice('');

				navigate("/courses");
			} else {
				Swal.fire({
				    title: "Something went wrong",
				    icon: "error",
				    text: "Please try again."
				})
			}

		})
	};

	useEffect(() => {
		if (name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [name, description, price]);


	return (
		(user.id === null || user.isAdmin === false) ?
			<Navigate to="/courses" />

		:
			<>
				<Form onSubmit={(event) => addNewCourse(event)}>
		        	<h1 className="my-5 text-center">Add New Course</h1>

			            <Form.Group>
			                <Form.Label>Name:</Form.Label>
			                <Form.Control 
			                	type="text" 
			                	placeholder="Enter Name" 
			                	// this is called 2way data binding - meaning kinakabit mo isang state to a specific input field. using value, and onChange
			                	value ={name}
			                	onChange={event => {setName(event.target.value)}}
			                />
			            </Form.Group>

			            <Form.Group>
			                <Form.Label>Description</Form.Label>
			                <Form.Control 
				                type="text" 
				                placeholder="Enter Description" 
				                required
				                value ={description}
			                	onChange={event => {setDescription(event.target.value)}}
			                />
			            </Form.Group>

			            <Form.Group>
			                <Form.Label>Price:</Form.Label>
			                <Form.Control 
				                type="number" 
				                placeholder="Enter Price" 
				                required
				                value ={price}
			                	onChange={event => {setPrice(event.target.value)}}
			               	/>
			            </Form.Group>

		            	<Button variant="primary" type="submit" disabled = {isActive === false}>Submit</Button>
		        </Form>
	        </>
	)

}