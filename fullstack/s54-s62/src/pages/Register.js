import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js'

export default function Register() {

	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);


	function registerUser(event){
		event.preventDefault();//prevent default - para di na mag refresh browser pag nag submit, kase default option nya is pag nag submit ka ng form e matic mag rerefresh yung browser, so it is a badthing when it comes to web application. so much better if there's no loading pages when u do an event such as submitting.

		fetch('http://localhost:4000/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then(response => response.json()).then(result => {

			if (result){
				// resets all input fields
				setFirstName("")
				setLastName("")
				setEmail("")
				setMobileNo("")
				setPassword("")
				setConfirmPassword("")

				alert('New User Registered!')
			} else{
				alert('Please try again :<')
			}
		})

	}
	// Function that has 2 arguments, function and array. para syang side effect, analogy is like when you drink something ano yung side effect nya sayo. it will based on the 2nd argument which is the array. So if empty array andyan, firstload lang, mag rurun agad yung use effect natin. upon the initial load of component. So ano nga ba dapat laman ni array. so mga nilalagay dyan is mga state. so every time na may changes dun sa state na nilagay mo sa argument na array, mag iistart yung useeffect na function. 
	// Note: If the 2nd argument is empty, then the function will only run upon the initial loading of the component.
	useEffect(() => {
		// Checks if all fields aren't empty, if password and confirmPassword fields are matching, and mobile number is 11 characters. 
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password, confirmPassword]);

	return (
		(user.id !== null) ?
		    <Navigate to="/courses" />
		:
		<>
			<Form onSubmit={(event) => registerUser(event)}>
	        	<h1 className="my-5 text-center">Register</h1>
		            <Form.Group>
		                <Form.Label>First Name:</Form.Label>
		                <Form.Control 
		                	type="text" 
		                	placeholder="Enter First Name" 
		                	// this is called 2way data binding - meaning kinakabit mo isang state to a specific input field. using value, and onChange
		                	value ={firstName}
		                	onChange={event => {setFirstName(event.target.value)}}
		                />
		            </Form.Group>
		            <Form.Group>
		                <Form.Label>Last Name:</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Last Name" 
			                required
			                value ={lastName}
		                	onChange={event => {setLastName(event.target.value)}}
		                />
		            </Form.Group>
		            <Form.Group>
		                <Form.Label>Email:</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter Email" 
			                required
			                value ={email}
		                	onChange={event => {setEmail(event.target.value)}}
		               	/>
		            </Form.Group>
		            <Form.Group>
		                <Form.Label>Mobile No:</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter 11 Digit No." 
			                required
			                value ={mobileNo}
		                	onChange={event => {setMobileNo(event.target.value)}}
		                />
		            </Form.Group>
		            <Form.Group>
		                <Form.Label>Password:</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Enter Password" 
			                required
			                value ={password}
		                	onChange={event => {setPassword(event.target.value)}}
		                />
		            </Form.Group>
		            <Form.Group>
		                <Form.Label>Confirm Password:</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Confirm Password" 
			                required
			                value ={confirmPassword}
		                	onChange={event => {setConfirmPassword(event.target.value)}}
		                />
		            </Form.Group>

	            	<Button variant="primary" type="submit" disabled = {isActive === false}>Submit</Button>
	        </Form>
        </>
	)
}