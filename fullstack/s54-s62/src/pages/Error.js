import Banner from '../components/Banner.js';

export default function Error() {
  return (
    <>
      <Banner 
      	title="404 - Not Found" 
      	subtitle="The page you're looking for cannot be found."
      	buttonText="Back Home"
      	buttonLink="/"
      />
    </>
  );
}