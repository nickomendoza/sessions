import React, { useState } from 'react';
import { Container, Row, Col, Form, Button, ListGroup } from 'react-bootstrap';

export default function CourseSearch() {
    const [minPrice, setMinPrice] = useState('');
    const [maxPrice, setMaxPrice] = useState('');
    const [courses, setCourses] = useState([]);

    const handleSearch = () => {
        const requestData = {
            minPrice: Number(minPrice),
            maxPrice: Number(maxPrice)
        };

        fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestData),
        })
        .then(response => response.json())
        .then(data => {
            if (Array.isArray(data.courses)) {
                setCourses(data.courses);
            } else {
                console.error("Received data is not an array:", data);
            }
        })
        .catch(error => {
            console.error("Error fetching courses:", error);
        });
    };

    return (
        <Container>
            <Row className="mt-4">
                <Col>
                    <h2>Search Courses</h2>
                    <Form>
                        <Form.Group>
                            <Form.Label>Min Price</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Enter Min Price"
                                value={minPrice}
                                onChange={e => setMinPrice(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Max Price</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Enter Max Price"
                                value={maxPrice}
                                onChange={e => setMaxPrice(e.target.value)}
                            />
                        </Form.Group>
                        <Button variant="primary" onClick={handleSearch}>
                            Search
                        </Button>
                    </Form>
                </Col>
            </Row>
            <Row className="mt-4">
                <Col>
                    <h3>Results</h3>
                    <ListGroup>
                        {courses.map(course => (
                            <ListGroup.Item key={course._id}>
                                {course.name} - PHP {course.price}
                            </ListGroup.Item>
                        ))}
                    </ListGroup>
                </Col>
            </Row>
        </Container>
    );
}
