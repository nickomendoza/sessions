import { Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({ title, subtitle, buttonText, buttonLink }){
	return(
		<Row>
			<Col className ="p-5 text-center">
				<h1>{title}</h1>
				<p>{subtitle}</p>
				<Button variant="primary" as={Link} to={buttonLink}>
					{buttonText}
				</Button>
			</Col>
		</Row>

	)
}