import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
// import { useState } from 'react';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){

	// Destructuring the contents of 'course'
	const {name, description, price, _id} = courseProp;

	// //const [getter , setter]
	// // getters - yan yung gagamitin natin para makuha yung value ng isang bagay. tas makukuha yung value nya sa loob ng useState
	// // setters - ginagamit to update and modify yung value ni count. Naming convention nya e laging may 'set' sa harap tas camel casing.
	// const [count, setCount] = useState(0);
	// // Activity s55
	// const [seat, setSeat] = useState(30);

	// function enroll () {
	// 	if(seat > 0){
	// 		setCount(prev_value => prev_value + 1);
	// 		// Activity s55
	// 		setSeat(prev_value=> prev_value - 1);
	// 	}
	// 	// So using the setter which is setCount, we execute this operation where in using the prev_value then add 1 then magiging 1 na value nya. 
	// 	else {
	// 		alert('No more seats available.');
	// 	}
	// }

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				
				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>

	)
}

// PropTypes is used for validating the data from the props.
// So bale magkakaroon ka access sa property nung structure, then ayun using the 'course' na props then .shape is to determine or to set a property or structure that will be followed, so using that we can customize the validation in each every data. its just to validate kung ano yugn pumapasok na data sa front end. 
// so if ever na missing or isa dyan ay empty, may warning na lalabas but still the function will continue, but in the specific property missing will be displaying a warning
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
