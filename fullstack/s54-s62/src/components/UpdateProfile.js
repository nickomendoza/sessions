import React, { useState } from 'react';
import Swal from 'sweetalert2';
import {Button, Form} from 'react-bootstrap';

export default function ProfileUpdate() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');

    const handleUpdate = () => {
        // Prepare the request data
        const requestData = {
            firstName: firstName,
            lastName: lastName,
            mobileNo: mobileNo,
        };

        // Make the API request to update the profile
        fetch(`${process.env.REACT_APP_API_URL}/users/profile/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(requestData),
        })
        .then(response => response.json())
        .then(data => {
            if (data) {
                Swal.fire({
                    title: 'Success',
                    text: 'Profile updated successfully.',
                    icon: 'success'
                });
            } else {
                Swal.fire({
                    title: 'Error',
                    text: 'Failed to update profile. Please try again.',
                    icon: 'error',
                });
            }
        })
    };

    return (
           <div className="mt-4">
               <h2>Update Profile</h2>
               <Form>
                   <Form.Group>
                       <Form.Label>First Name</Form.Label>
                       <Form.Control
                           type="text"
                           value={firstName}
                           onChange={e => setFirstName(e.target.value)}
                       />
                   </Form.Group>

                   <Form.Group>
                       <Form.Label>Last Name</Form.Label>
                       <Form.Control
                           type="text"
                           value={lastName}
                           onChange={e => setLastName(e.target.value)}
                       />
                   </Form.Group>

                   <Form.Group>
                       <Form.Label>Mobile No</Form.Label>
                       <Form.Control
                           type="text"
                           value={mobileNo}
                           onChange={e => setMobileNo(e.target.value)}
                       />
                   </Form.Group>

                   <Button variant="primary" onClick={handleUpdate}>
                       Update
                   </Button>
               </Form>
           </div>
    );
}
