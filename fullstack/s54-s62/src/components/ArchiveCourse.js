import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse ({ courseId, isActive, fetchData }) {

	const archiveToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course archived successfully.'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}

	const activateToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/activate`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course activated successfully.'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}

	return (
		<>
			{ (isActive === true) ?
				<Button variant="danger" onClick={archiveToggle}>Archive</Button>
				:
				<Button variant="success" onClick={activateToggle}>Activate</Button>
			}

		</>



	)
}