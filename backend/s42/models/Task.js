const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});
// MONGOOSE SCHEMA - it is a class na pwede lagyan ng laman which is the object, tas andon yung properties nya. so after nya macreate, yun yung magiging structure ng database document na ginawa mo.
// MODELS - capital at nakasingular lagi naming convention nya. ang result nya will be 'tasks'. so bale to ginagamit para gumawa ng document where in gagamitin nya si taskSchema as the basis of the structure of your document.

module.exports = mongoose.model("Task", taskSchema);