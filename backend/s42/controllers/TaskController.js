// usually dito iniimport yung model sa controller
const Task = require('../models/Task.js')



module.exports.getAllTasks = () => {
	return Task.find({}).then((result, error) => {
		if (error){
			return { message: error.message }
		}

		return { tasks: result }
	})
}

module.exports.createTask = (request_body) => {
	// findOne - kung ano laman nung nasa object, it will find a task na equal property na nakalagay.
	return Task.findOne({name: request_body.name}).then((result, error) => {
		if(result != null && result.name == request_body.name){
			return {message: "Duplicate task found!"};
		} else {
			// 1. create new instance of the task model which will contain the properties required based on the Schema
			let newTask = new Task({
				name: request_body.name
			});
			// 2. save the new task to the database
			return newTask.save().then((savedTask, error) => {
				if(error){
					return { message: error.message };
				}

				return {message: 'New task created!'};
			})
		}
	})
}

module.exports.updateStatus = (taskId) => {
	return Task.findByIdAndUpdate(taskId, {status: 'complete'}, {new: true}).then((result, error) => {
		if (!result) {
			return { message: 'Task not found!'};
		} 

		return { updatedTask: result }
	})
}