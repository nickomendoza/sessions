// Server Variables

const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() // initialization of dotenv package which contains sensitive info
const taskRoutes = require('./routes/taskRoute.js');
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-mendoza.klhsbzp.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let database = mongoose.connection;
database.on('error', () => console.log('Connection error :('));
database.once('open', () => console.log('Connected to MongoDB!'));

// Midleware

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/api/tasks', taskRoutes); // Initializing the routes for /tasks so the server will know the routes available to send request to.


// Server Listening

app.listen(port, () => console.log(`Server is running at local host ${port}`));

module.exports = app;
