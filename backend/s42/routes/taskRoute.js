const express = require('express');

const router = express.Router(); // this will be the one who will establish our router

// Always naka pascal naming sa mga controllers.
const TaskController = require('../controllers/TaskController.js');
// Insert Routes here
router.post('/', (request, response) => {
	TaskController.createTask(request.body).then(result => {
		response.send(result);
	})
})
// getting all of the task
router.get('/', (request, response) => {
	TaskController.getAllTasks().then(result => {
		response.send(result);
	})
}) 

// update task
router.put('/:id/complete', (request, response) => {
	TaskController.updateStatus(request.params.id).then(result => {
		response.send(result)
	})
})

module.exports = router;