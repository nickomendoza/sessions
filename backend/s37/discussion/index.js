

// We use 'require' directive to load the Node.js modules
// a module is a software component or part of a program that contains one or more routines
// with "http" module lets Node.js transfer data using the HYPER TEXT TRANSFER PROTOCOL.
// HTTP IS A PROTOCOL ALLOWS FETCHING OF RESOURCES LIKE HTML DOCUMENTS.
let http = require ("http");

// .createServer() method creates an HTTP server that listen to requests on a specific port and gives response back to the client.
// we can also use req, res only, they cab be change. the first will be request and the nextone will be the response. 
// 4000 is the port where the server will listen. A port is a virtual point where the network conncetion start and end.
http.createServer(function(request, response){

	// .writeHead method po 
	// 200 - status code for the response - means OK
	// sets the content-type of the response to be a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// .end sends the response with the text content 'HelloWorld'
	response.end('Hello World');
}).listen(4000)

// When server is running, console will print the message:
console.log('Server is running at port 4000');