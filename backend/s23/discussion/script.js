// If-else Statements

let number = 1

if (number > 1) {
	console.log("The number is greater than 1! :>");
} else if (number < 1) {
	console.log("The number is less than 1! :<");
} else {
	console.log("None of the conditions were true. T_T");
}

// Falsey values
if (false) {
	console.log("Falsey");
}
if (0) {
	console.log("Falsey");
}
if (undefined) {
	console.log("Falsey");
}

// Truthy values
if (true) {
	console.log("Truthy");
}
if (1) {
	console.log("Truthy");
}
if ([]) {
	console.log("Truthy");
}

// Ternary Operators
let result = (1 < 10) ? true : false;
console.log ("Value returned from the ternary operator is " + result);

// kapag may condition kayo na may 2 or more line sa loob, use if instead. ex.

if (5==5) {
	let greeting = "hello";
	console.log(greeting);
}

// SWITCH STATEMENT
let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
	case 'monday':
		console.log("The day today is Monday!");
	break;
	case 'tuesday':
		console.log("The day today is Tuesday!");
	break;
	case 'wednesday':
		console.log("The day today is Wednesday!");
	break;
	case 'thursday':
		console.log("The day today is Thursday!");
	break;
	case 'friday':
		console.log("The day today is Friday!");
	break;
	case 'saturday':
		console.log("The day today is Saturday!");
	break;
	case 'sunday':
		console.log("The day today is Sunday!");
	break;
	default:
		console.log ("hoy input a valid day naman bruuh");
		break;
}

// TRY-CATCH-FINALLY STATEMENT
function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	} catch (error){
		console.log(error.message);
	}finally {
		alert("Intensity updates will show new alert!");
	}
}
showIntensityAlert(56);