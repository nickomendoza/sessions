// Server Variables

const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() // initialization of dotenv package which contains sensitive info
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-mendoza.klhsbzp.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let database = mongoose.connection;
database.on('error', () => console.log('Connection error :('));
database.once('open', () => console.log('Connected to MongoDB!'));

// Midleware

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// MONGOOSE SCHEMA - it is a class na pwede lagyan ng laman which is the object, tas andon yung properties nya. so after nya macreate, yun yung magiging structure ng database document na ginawa mo.
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// MODELS - capital at nakasingular lagi naming convention nya. ang result nya will be 'tasks'. so bale to ginagamit para gumawa ng document where in gagamitin nya si taskSchema as the basis of the structure of your document.
const Task = mongoose.model("Task", taskSchema);

// ROUTES
// Creating a new task
app.post('/tasks', (request, response) => {
	// findOne - kung ano laman nung nasa object, it will find a task na equal property na nakalagay.
	Task.findOne({name: request.body.name}).then((result, error) => {
		if(result != null && result.name == request.body.name){
			return response.send("Duplicate task found!");
		} else {
			// 1. create new instance of the task model which will contain the properties required based on the Schema
			let newTask = new Task({
				name: request.body.name
			});
			// 2. save the new task to the database
			newTask.save().then((savedTask, error) => {
				if(error){
					return response.send({ message: error.message });
				}

				return response.send(201, 'New task created!');
			})
		}
	})
})
// getting all of the task
app.get('/tasks', (request, response) => {
	Task.find({}).then((result, error) => {
		if (error){
			return response.send({ message: error.message});
		}

		return response.status(200).json({tasks: result});
	})
})

// Activity 

// Schema
let userSchema = new 

// Server Listening

app.listen(port, () => console.log(`Server is running at local host ${port}`));

module.exports = app;


/*else {
			// 1. create new instance of the task model which will contain the properties required based on the Schema
			let newTask = new Task({
				name: request.body.name
			});
			// 2. save the new task to the database
			newTask.save().then((savedTask, error) => {
				if(error){
					return response.send({ message: error.message });
				}

				return response.send(201, 'New task created!');
			})*/