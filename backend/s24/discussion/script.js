// WHILE LOOP
// let count=5;
// while (count !== 0) {
// 	console.log("Current value of count : " + count);
// 	count--;
// }

// DO-WHILE LOOP
	/*yung 'Number' na yan, yan yung magcoconvert sa input data galing kay prompt na laging string diba.*/
// let number = Number(prompt("Give me a number: "));

// do {
// 	console.log("Current value of number: " + number);

// 	number += 1;
// } while (number < 10)

// FOR LOOP
for(let count = 0; count <= 20; count++){
	console.log("Current for loop value: " + count);
}


// let my_string = "nicko andrei"
// // to get the length of a string
// console.log(my_string.length);
// // to get a specific letter in a string
// console.log(my_string[2]);


// // Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
// for(let index = 0; index < my_string.length; index++){
// 	console.log(my_string[index]);
// 	// it will keep printing each letter until it meets the last letter.
// }

// // MINI ACTIVITY
// let my_name = "nicko andrei mendoza";
// for(let index = 0; index < my_name.length; index++){
// 	if (my_name[index] == 'a' || 
// 		my_name[index] == 'e' ||
// 		) {
// 		console.log(my_name[index]);
// 	}
// }