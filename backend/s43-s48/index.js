// Server Variables
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors'); //cross origin resource sharing - this will allows our hosted front-end app to send requests to this server.
const port = 4000;
const userRoutes = require('./routes/userRoutes.js');
require('dotenv').config()
const courseRoutes = require('./routes/courseRoutes.js');
const app = express();

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// Routes
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);



// Database Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-mendoza.klhsbzp.mongodb.net/b303-booking-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log("Can't connect to database."));
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));

	// Snce w're hosting this API in the cloud, for port that will be provided by the cloud if port 4000 is not available
app.listen(process.env.PORT || port, () => {
	console.log(`Booking System API is now running at localhost:${process.env.PORT || port}`);
});

module.exports = app;
