const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

// Check if email exists
router.post('/check-email', (request, response) => {
	UserController.checkEmailExist(request.body).then((result) => {
		response.send(result);
	})
})
// Register User
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})


// login user
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

//get user details
// auth.verify - tawag dyan e middleware din, kumbaga pag nag request ka then papasok muna yung details sa auth.verify, then mag rurun yung function na verify sa auth, then mag momove on lang sya sa process pag natawag na yung next or meaning verified na sya. pasok sa request yung details then pasok sa authverify tas pag verified na sya then tsaka mag rurun yung arrow function.
router.post('/details', auth.verify, auth.verifyAdmin,(request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})

// enroll user to a course
router.post('/enroll', auth.verify, (request, response) => {
	UserController.enroll(request, response);
})

// Get user Enrollments

router.get('/enrollments', auth.verify, (request, response) => {
	UserController.getEnrollments(request, response);
})



module.exports = router;