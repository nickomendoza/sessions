const express = require('express')
const router = express.Router();
const auth = require('../auth.js')
const CourseController = require('../controllers/CourseController.js')

// you can destructure the 'auth' variable to extract the function being exported from it. you can then use the functions directly without having to use dot notation
const {verify, verifyAdmin} = auth;


// Create Single course
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request, response);
})

// Get all courses
router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
})

// get all active courses
router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response);
})

// get specific course
router.get('/:id', (request, response) => {
	CourseController.getCourse(request, response);
})

// Update single course
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})

// Activity
// Archiving

router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) =>{
	CourseController.archiveCourse(request, response);
})

router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.activateCourse(request, response);
})

// For searching course by name

router.post('/search', (request, response) =>{
	CourseController.searchCourses(request, response);
});

module.exports = router; 
