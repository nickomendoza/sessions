const Course = require('../models/Course.js');

module.exports.addCourse = (request, response) => {
	let new_course = new Course({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	})

	return new_course.save().then((saved_course, error) => {
		if (error){
			return response.send(false);
		}

		return response.send(true)


	}).catch(error => console.log(error));
}
// Get All
// pag empty curly braces, meaning kukunin nya lahat kasi wala tayong sinet na kukuning info.
module.exports.getAllCourses = (request, response) => {
	return Course.find({}).then(result => {
		return response.send(result);
	})
}

// Get all Active
module.exports.getAllActiveCourses = (request, response) => {
	return Course.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

module.exports.getCourse = (request, response) => {
	return Course.findById(request.params.id).then(result => {
		return response.send(result);
	})
}
	
// for update single course

module.exports.updateCourse = (request, response) => {
	let updated_course_details = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course, error) => {
		if (error){
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Course has been updated successfully!'
		})
	})
}

// Activity
module.exports.archiveCourse = (request, response) => {
	let course_status = {
		isActive: 'false'
	};
	return Course.findByIdAndUpdate(request.params.id, course_status).then((result, error) =>{
		if (error) {
			return response.send({
				message: error.message
			})
		}

		return response.send(true);
	})
}

module.exports.activateCourse = (request, response) => {
	let course_status = {
		isActive: 'true'
	};

	return Course.findByIdAndUpdate(request.params.id, course_status).then((result, error) =>{
		if (error){
			return response.send({
				message: error.message
			})
		}

		return response.send(true);
	})
}

module.exports.searchCourses = (request, response) => {
    
      const courseName = request.body.courseName;
      return Course.find({ name: { $regex: courseName, $options: 'i' } }).then((courses) => {
      	response.send(courses)
      }).catch(error => response.send({
      	message: error.message
      }))
  }