let http = require('http');

const port = 4000;

// Mock Database
let users = [
		{
			"name": "Paolo",
			"email": "Paolo@gmail.com"
		},
		{
			"name": "Nicko",
			"email": "nicko@gmail.com"
		}

	]

const app = http.createServer((req, res) => {

	//  /items GET route
	if(req.url == '/items' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Data retrieved from the database')
	}

	// /items POST route
	if (req.url == '/items' && req.method == 'POST'){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('Data send to the database!');
	}

	// getting all the items from mock database

	if (req.url == '/users' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.end(JSON.stringify(users));
	}

	// creating a user in our database
	if (req.url == '/users' && req.method == 'POST'){
		// placeholder for the data that is within the request, sya yung sasalo ng strings.
		let request_body = '';

		// the '.on' function listen for specific behavior within the request. In this example, we are checking for when the request detects data within it. once the request receives data, it will run a function that will handle that data in its parameter.
		req.on('data', (data) => {
			// assigns the data from the request, to the 'request_body' variable
			request_body += data; 
		})
		req.on('end', () => {
			// to check the data type of the request_body variable (by default it will be JSON string)
			console.log(typeof request_body);

			// converting JSON string into regular javascript format
			request_body = JSON.parse(request_body);

			// creating a new_user object to contain the data from the request_body
			let new_user = {
				"name" : request_body.name, // the dot notation is possible since the request_body has been converted to javascirpt format
				"email": request_body.email
			}


			
			users.push(new_user);

			res.end(JSON.stringify(new_user));
		})
	}

})

// .listen can have one more argument
app.listen(port, () => console.log(`Server is running at local host:${port}`));
