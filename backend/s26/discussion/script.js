// ARRAYS AND INDEXES
let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu", "Lenovo"];
let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative way to write arrays
let my_tasks = [
	"drink html",
	"eat javascript",
	"inhale CSS",
	"bake sass"
	];

// Reassigning values
console.log("Array before reassignment");
console.log(my_tasks);
// To reassign a value in an array, just use its index number and use an assignment operator to replace the value of that index.
my_tasks[2] = "run hello world";
console.log("Array after reassignment");
console.log(my_tasks);

// Reading from Arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the length of an array
console.log(computer_brands.length);

// Accessing Last element in an array
let index_of_last_element = computer_brands.length - 1;
console.log(computer_brands[index_of_last_element]);

// Array Methods or Array Functions
let fruits = ["Apple", "Orange", "Kiwi", "Passionfruit"];
console.log("Current Array: ");
console.log(fruits);

// Push method - '.push()' method or function is responsible adding an item to our array.
fruits.push("Mango", "Cocomelon");
console.log("Updated array after push method:");
console.log(fruits);

// Pop Method '.pop()' - remove the last item of the array. pwede mo din sya ilagay sa isang variable, then yun yung magsisilbing container na malalagay dun yung removed element sa array. 
console.log("Current Array: ");
console.log(fruits);

let removed_item = fruits.pop();
console.log("Updated array after pop method:");
console.log(fruits);
console.log("Removed fruit: " + removed_item);

// Unshift Method '.unshift()' - Adding Items in the beginning of the array.

console.log("Current Array: ");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method:");
console.log(fruits);

// Shift Method '.shift()' - removes item at the beginning of the array.
console.log("Current Array: ");
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method:");
console.log(fruits);

// Splice Method '.splice()' - can simultaneously update multiple items in an array starting from a specific index.
console.log("Current Array: ");
console.log(fruits);

// splice(startingIndex, numberOfItemsToBeDeleted, itemsToBeAdded)
fruits.splice(1, 2, 'Lime', 'Cherry');

console.log("Updated array after splice method:");
console.log(fruits);

// Sort Method '.sort()' - arrange the index items either ascending or descending order. for the different functions and other things regards to this, visist https://www.w3schools.com/jsref/jsref_sort.asp

console.log("Current Array: ");
console.log(fruits);


fruits.sort();
// fruits.reverse();
console.log("Updated array after sort method:");
console.log(fruits);


// NON-MUTATOR METHODS - Every method written above is called a 'Mutator method' bcoz it modifies the value of the array one way or another. Non-mutator Methods on the other hand, dont do the same thing, they instead execute specific functionalities that can be done with the existing array values.\


// '.indexOf()' - this will particularly get the index number of a specific item in array.
let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of lenovo is: " + index_of_lenovo);

// '.lastIndexOf()' - gets the index of a specific item starting from the end of the array. This will only work if there are multiple instances of the same item. The item closest to the end of the array will be the one that the method will get the index of.
let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of lenovo starting from the end of the array is: " + index_of_lenovo_from_last_item);

// Slice method '.slice(indexnumber);'

let hobbies = ["Gaming", "Running", "Cheating", "Cycling", "Writing"];

// by putting '2' as the argument, we are starting the slicing process from the item with the index of 2
console.log(hobbies);
let sliced_array_from_hobbies = hobbies.slice(2); 
console.log(sliced_array_from_hobbies);

// By putting two arguments instead of one, we are also specifying where the slice will end.
let sliced_array_from_hobbies_B = hobbies.slice(2, 4); 
console.log(sliced_array_from_hobbies_B);

// By using a negative number as the index, the count where it will start with will come from the end of the array instead of the beginning.
let sliced_array_from_hobbies_C = hobbies.slice(-2); 
console.log(sliced_array_from_hobbies_C);

// toString Method .toString()
let string_array = hobbies.toString();
console.log(string_array);

// concat Method .concat()
let greeting = ["hello", "Word"];
let exclamation = ["!", "?"];
let concat_greeting = greeting.concat(exclamation);
console.log(concat_greeting);

// join Method .join() - this will convert the array to a strings, this will serve and insert a seperator between them as defined in the join function.
console.log(hobbies.join(' - '));


// foreach Method
hobbies.forEach(function(hobby){
	console.log(hobby);
})

// map Method

let numbers_list = [1, 2, 3, 4, 5];
let numbers_map = numbers_list.map(function(number){
	return number * 2
})
console.log(numbers_map);

// filter Method

let filtered_numbers = numbers_list.filter(function(number){
	return (number < 3);
})
console.log(filtered_numbers);

// Multi-dimensional Arrays
let chess_board = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chess_board[1][4]);