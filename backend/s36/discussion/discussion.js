// using the aggreagate method
	// $match - is used to match or get documents that satisfies the condition. it is similar to find(). You can use query operators to make your criteria more flexible. pwede ka maglagay like lt lte gt gte.
	/*
		$match -> Apple, Kiwi, Banana
	*/
db.fruits.aggregate([
		{$match: {onSale: true}},
	// $group - allows us to group together documents and create an analysis out of the group elements.
		// _id: $supplier_id
		// $sum - used to add or total the values in a given field.
		{$group: {_id:"$supplier_id", total: {$sum: "$stock"}}}
	]);



db.fruits.aggregate([
	/*
		$match - Apple, Kiwi, Banana. They were onSale:true
	*/
		{$match: {onSale: true}},
	/*
		$supplier_id
		Apple - 1.0
		Kiwi - 1.0
		Banana - 2.0

		_id: 1.0
			avgStocks: average stocks of fruits in 1.0
			avgStocks: (apple stocks + kiwi stocks) / 2
			avgStocks: (20+25)/2
			avgStocks: 22.5
		_id: 2.0 
			avgStocks: average stocks of fruits in 2.0
			avgStocks: Banana stocks / 1
			avgStocks: 15/1
			avgStocks: 15
	*/
	// $avg - gets the average of the values of the given field per group
		{$group: {_id: '$supplier_id', avgStocks: {$avg: '$stock'}}}
	// $project - can be used when "aggregating" data to include/exclude fields from the returned result. (field projection)
		{$project: {_id: 0}}

	]);


db.fruits.aggregate([
	/*
		$match - Apple, Kiwi, Banana. They were onSale:true
	*/
		{$match: {onSale: true}},
	/*
		$supplier_id
		Apple - 1.0
		Kiwi - 1.0
		Banana - 2.0

		_id: 1.0
			maxPrice - finds the highest price of the fruit in the group
			maxPrice: Apple Price vs Kiwi Price
			maxPrice: 40 vs 50
			maxPrice: 50
		_id: 2.0 
		maxPrice - finds the highest price of the fruit in the group
			maxPrice: Banana
			maxPrice: 20
			maxPrice: 50
	*/
		{$group: {_id: '$supplier_id', maxPrice: {$max: '$price'}}},
	// $sort - used to change the order of the aggregated result, either 1 or -1.
	// -1 - will sort the aggregated results in a reverse order
	// you will declare if want you would like, for ex. _id or price
		{$sort: {maxPrice: -1}}
	]);

db.fruits.aggregate([
	// $unwind - deconstruct the array field from a collection with an array value to output result for each element.
		{$unwind: "$origin"}
	]);

db.fruits.aggregate([
	{$unwind: '$origin'},
	// sum: 1 - 
	{$group: {_id: '$origin', kinds: {$sum: 1}}}
	]);


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: '$supplier_id', max_price: {$max: '$price'}}},
	]);