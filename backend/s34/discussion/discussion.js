// inserting multiple documents at once

db.users.insertMany([
	{
		"firstName": "John",
		"lastName": "Doe"
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe"
	}



]);

// Retrieving all the inserted users

db.users.find();


// Retrieving a specific document from a collection
db.users.find({"firstName": "John"});

// [SECTION] UPDATING EXISTING DOCUMENTS
db.users.updateOne(
	{
		"_id": ObjectId("64c1c49510882b2ee52be239")
	},
	{
		$set: {
			"lastName": "Gaza"
		}
	}

);

// For updating multiple documents
db.users.updateMany(
	{
		"lastName": "Doe"	
	},
	{
		$set: {"firstName": "Mary"
		}
	}


);


// [SECTION] Deleting documents from a collection

// Deleting multiple documents
db.users.deleteMany({"lastName": "Doe"});

// Deleting single document
db.users.deleteOne({
	"_id": ObjectId("64c1c49510882b2ee52be239")
});