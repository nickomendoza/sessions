console.log("Mutator Methods");

// Iteration Methods


// forEach() - loops through the array
// map() - loops through the array and returns a new array
// filter() - returns a new array containing elements which meets the given condition

// every() - checks if all elements meet the given condition. return boolean true/false depends on the given condition.
let numbers = [1, 2, 3, 4, 5, 6];

let all_valid = numbers.every(function(number){
	return number > 3;

})

console.log("Result of every() method: ");
console.log(all_valid);

// some() - checks if atleast one element meets the given condition
let some_valid = numbers.some(function(number) {
	return number < 2;
})

console.log("result of some() method: ");
console.log(some_valid);

// includes() method - can be chained using them one after another.
let products = ["mouse", "keyboard", "Laptop", "monitor"];

let filtered_products = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log(filtered_products);

// reduc()
let iteration = 0;

let reduced_array = numbers.reduce(function(x,y){
	console.warn("current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("currentValue "+ y);

	return x+y;
})

console.log("result of reduce method: " + reduced_array);

let products_reduce = products.reduce(function(x,y) {
	return x + ' ' + y;
})

console.log("results of reduce() method: " + products_reduce);