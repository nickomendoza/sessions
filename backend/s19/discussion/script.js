// alert("Hello World!");

console.log("Hello World!");

// [SECTION] Variables

// Variable initialization, declaration, and invocation.
let my_variable = "Hola, Mundo!";
console.log(my_variable);

// Concatenating Strings
let country = "Philippines";
let province = "Metro Manila";

let full_address = province + ", " + country;
console.log(full_address);

// Integers

let headcount = 26;
let grade = 98.7
// let sum = headcount + grade;
console.log("The number of student is " +headcount+ " and the average grade of all student is "+grade);
// console.log(sum);

// boolean - the value of boolean is true or false only, when naming variable that have a boolean value, make sure that they are formatted like a question phrase.
let is_married = false;
let is_good_conduct = true;

console.log("He's married: " + is_married);
console.log("She's a good person: " + is_good_conduct);

// Arrays
let grades = [98.8, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(details);

// Objects
let person = {
	fullName: "Juan Dela Cruz",
	age: 40,
	isMarried: false,
	contact: ["0912390912", "0213909123"],
	address: {
		houseNumber: "345", 
		city: "England"
	}
}

// typeof - icoconvert sila kung ano data types nila. so bale kung ano data type nila yun ididisplay sa console.
// binabasa ni JS ang array as an object.

console.log(typeof person);

console.log(typeof grades);

// null and undefined
// null means wala talaga syang data, may variable tayo pero walang declared na laman. blangko.
// undefined - no actuall value. as in wala syang kahit ano. 
// usually di naman tayo mag dedeclare ng mga to, si javascript magbibigay nyan pag may mali tayong nilagay or error.
let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);

// [SECTION] Operators

// Arithmetic Operators
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;	

console.log("Sum: " + sum)
console.log("Difference: " + difference)
console.log("Product: " + product)
console.log("Quotient: " + quotient)
console.log("Remainder: " + remainder);

// Assignment Operators - responsible for assigning data to a variable.

let assignment_number = 0;
assignment_number = assignment_number + 2;
console.log("Result of addition assignment operator: " + assignment_number);

assignment_number += 2;
console.log("Result of shorthand addition assignment operator: " + assignment_number);
