// Function Declaration and Invocation

function printName(){
	console.log("My Name is Nicko");
}

printName();

// FUNCTION EXPRESSION

let variable_function = function(){
	console.log("Hello from Function Expression!")
}

variable_function();

// SCOPING 

let global_variable = "Im a worldwide from glabal_variable"

console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);
	// you can use the global variable inside any function as long as they were declared outside the function scope.
	console.log(global_variable);
}
// You cannot use locally scoped variables outside the function they are declared.
// console.log(function_variable);
showNames();

// NESTED FUNCTIONS

function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John";

		console.log(name);
		console.log(nested_name);
	}

	childFunction();
	// pwede mo maaccess yung kay parent pero bawal mo maaccess yung kay child. 
	// console.log(nested_name);
}

parentFunction();
// childFunction();

// BEST PRACTICES FOR FUNCTION NAMING

	// wag tipidin ang pagpapangalan sa isang function, as long as mas mauunawaan or yung function na yun
// function printWelcomeMessageForUser(){
// 	let first_name = prompt("Enter your first name: ");
// 	let last_name = prompt("Enter your last name: ")

// 	console.log("Hello, " + first_name + " " + last_name + "!");
// 	console.log("Welcome sa page ko!");
// }

// printWelcomeMessageForUser();


// RETURN STATEMENT

function fullName(){
	console.log("Hello nicko!")
}

console.log(fullName());