// SERVER variables for initialization
const express = require('express'); // Eto yung ginagamit natin to import the express tas nilalagay sa variable na 'exrpress'
const app = express(); // eto naman iniinvoke si express para magamit natin sya na mapupunta sa variable na 'app'. bale sya na yung mag rerepresent sa 'express'
const port = 4000; // this will serve as the port we will be using for our project.

// Middleware - para lang syang function na nag rurun within your application, every time na may specific na nangyayari magtitrigger yung middleware na to. bale nireregister natin sila as middleware 

app.use(express.json()); // nireregister natin si express.json() as middleware, ang purpose nya is sya yung responsible sa converting every json format into regular javascript. it will allow express to read. everytime na magrerequest tayo eto yung mag coconvert like parse json request. 
app.use(express.urlencoded({extended: true})); // this function allows express to received different types of data. not just default strings and arrays, it will allows express to be able to read objects etc.

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`))


// ROUTES
app.get('/', (request,response) => {
	response.send('Hello World!');
})
app.post('/greeting', (request, response) =>{
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!!`); //Note that you need to put a body on the postman, raw -> JSON
})

// Mock Database
let users = [];


// This will be simulating a register route, where in pwede mag send ng request kapag gusto gumawa ng bagong user sa database.
app.post('/register', (request, response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} has successfully registered to anti selos academy.`);
	} else {
		response.send('Please input both username and password.');
	}
})

// Getting all the list of users

app.get('/users', (request, response) => {
	response.send(users);
})

// Activity

app.get('/home', (request, response) => {
	response.send('Welcome to the homepage');
})
module.exports = app;

