// console.log("love, objects");

// OBJECTS - a data type that is used to represent realworld 				objects. which also create properties and methods/ 				functionalities.

// creating objects using initializers/objects '{}'
let cellphone = {
	name: "Nokia 3210",
	manufacutreDate : 1999
};

console.log("Result from creating objects using initializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);

// creatomg objects using a constructor function

function Laptop(name, manufacutreDate) {
	this.name = name;
	this.manufacutreDate = manufacutreDate;
} 

// multiple instance of an object using the "new" keyword
// this method is called instantiation
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using constructor function");
console.log(laptop);

let laptop2 = new Laptop('Macbook Air', 2020);
console.log("Result from creating objects using constructof function");
console.log(laptop2);

// ACCESSING OBJECT PROPERTIES

// using square bracket notation

console.log("Result from square bracket notation: " + laptop2['name']);
// using dot notation
console.log("Result from dot notation: " + laptop2.name);

// access array objects

let array = [laptop, laptop2];

console.log(array[0]['name']);
console.log(array[1].name);

// Adding/Deleting/Reassigning Object Properties

let car = {};

// adding object properties using dot notation 'objectName.key = 'value'
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

// adding object properties using square bracket notation
car['manufacturing date'] = 2019;
console.log(car['manufacturing date']);
console.log(car['Manufacturing Date']);
// we cannot access the object property using dot notation if the key has spaces.
// console.log(car.manufacutring date);
console.log("Result from adding properties using the square bracket notation: ")
console.log(car);

// deleting object properties 'delete variableofobject[keyoftheobjectyouwanttodelete]'
delete car['manufacturing date'];
console.log("Result from deleting properties: ")
console.log(car);
// deleting object using dot notation
// delete car.manufactureingDate

// Reassigning Object Properties
// note: when reassigning property make sure that it is correct, because it is case sensitive.
car.name = "Honda Civic Type R";
console.log("Result from reassigning properties: ")
console.log(car);

// OBJECT METHODS - method is a function which acts a property of an object. Our properties now contain functions.

let person = {
	name: "Barbie",
	greet: function(){
		// console.log("Hello! My name is " + person.name);
		console.log("Hello! My name is " + this.name);
	}
}
console.log(person);
console.log("Result from object methods");
// greet() is now called a method.
person.greet();

// adding methods to objects
person.walk = function () {
	console.log(this.name + " walked 25 steps forward");
}

person.walk();

let friend = {
	// values can be strings, numbers, arrays, objects.
	name: "Ken",
	address: { 
		city:"Austin",
		state:"Texas",
		country:"USA"
	},
	email: ['ken@gmail.com', 'ken@mail.com'],
	introduce: function(person) {
		console.log("Nice to meet you "+ person.name + " I am " + this.name + " from " + this.address.city + " " + this.address.state);
	}
}

friend.introduce(person);