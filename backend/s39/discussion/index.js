console.log(fetch('https://jsonplaceholder.typicode.com/posts'));
// Synchronous way
fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => response.json()) // converts the JSON string from the response into regular javascript format.
	.then(posts => console.log(posts));

// Asynchronous way. - pag kasi gumamit ka ng ganito maaring mag tuloy tuloy sya. problema is yung part sa fetch(). Kase hindi parepareho bilis ng data or internet natin so it will take time from this fetch to get the data. if thats the case, maaring ituloy ni javascript without waiting to that fetch. so maaring magkaproblema. unlike sa then, may waiting na nangyayare. pero may way is use async await.
// function fetchData() {
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
// 	let json_result = await result.json();

// 	console.log(json_result);
// }
// fetchData();



// Adding headers, body, and method to the fetch() function
// CREATING A POST - hindi na need ng id.
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "New post!",
		body: "Hello World.",
		userId: 2
	})
})

.then(response => response.json())
.then(created_post => console.log(created_post));


// UPDATING EXISTING POST - kailangan specified yung id
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Corrected Post!"
	})
})

.then(response => response.json())
.then(updated_post => console.log(updated_post));

// DELETING EXISTING POST
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(response => response.json())
.then(deleted_post => console.log(deleted_post));

// FILTERING POSTS
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(response => response.json())
.then(post => console.log(post));

// GETTING COMMENTS OF A POST
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(response => response.json())
.then(comments => console.log(comments));