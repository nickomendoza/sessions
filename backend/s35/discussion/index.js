// Greater than operator - $gt or Greater than or equal - gte
db.users.find({
	age: {
		$gte:82
	}
})

// Less than operator -lt/ less than or equal - lte
db.users.find({
	age:{
		$lte: 82
	}
})

// REGEX operator
db.users.find({
	firstName: {
		$regex: 's',
		$options: 'i'
	}
})

db.users.find({
	lastName: {
		$regex: 'T',
		$options: 'i'
	}
})

// Combining all operators

db.users.find({
	age: {
		$gt:70
	},
	lastName: {
		$regex: 'g'
	}
})

db.users.find({
	age: {
		$lte: 76
	},
	firstName: {
		$regex: 'j',
		$options: 'i'
	}
})

// Field Projection
db.users.find({}, {
	"_id": 1
})

db.users.find({}, {
	"_id": 0,
	"firstName": 1
})

// Find users with letter s in their first name or d in their last name.

db.users.find({
	$or: [
			{firstName:{$regex:'s', $options:'i'}},
			{lastName:{$regex:'d', $options:'i'}}
		]
},
{
	"_id":0,
	"firstName":1,
	"lastName":1
})

// Result of querying users with the letter e in their last name and has an age of less than or equal to 30.
db.users.find({
    $and: [
        { age: {$lte: 30} },
        { lastName: {$regex: 'e'} }
    ]
})
// Result of querying users who are from the HR department and their age is greater than or equal to 70.
db.users.find({
	$and:[
		{age: {$gte:70}},
		{department: "HR"}
	]
})
